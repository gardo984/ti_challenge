
import pytest
from typing import List
from statistics_app import (
    DataCaptureException,
    DataCaptureStats,
    DataCapture,
)


class Helper:
    """
    class method that provides helper methods for
    unit tests.
    """

    @classmethod
    def get_options(cls) -> List[int]:
        return [
            3, 9, 3, 4, 6,
        ]

    @classmethod
    def load_data(cls) -> DataCapture:
        instance = DataCapture()
        for item in cls.get_options():
            instance.add(item)
        return instance


def test_statistics_app_add():
    item = DataCapture()
    output = item.add(3)
    assert(output is None)


def test_statistics_app_add_letter():
    item = DataCapture()
    try:
        _ = item.add('a')
    except DataCaptureException as ex:
        assert(isinstance(ex, DataCaptureException))


def test_statistics_app_add_negative_int():
    item = DataCapture()
    try:
        _ = item.add(-10)
    except DataCaptureException as ex:
        assert(isinstance(ex, DataCaptureException))


def test_statistics_app_build_stats():
    instance = Helper.load_data()
    stats = instance.build_stats()
    assert(isinstance(stats, DataCaptureStats))


def test_statistics_app_less():
    instance = Helper.load_data()
    stats = instance.build_stats()
    output = stats.less(9999)
    assert(isinstance(output, int) is True)
    assert(output > 0)


def test_statistics_app_greater():
    instance = Helper.load_data()
    stats = instance.build_stats()
    output = stats.greater(1)
    assert(isinstance(output, int) is True)
    assert(output > 0)


def test_statistics_app_between_exception():
    instance = Helper.load_data()
    stats = instance.build_stats()
    try:
        _ = stats.between(6, 3)
    except DataCaptureException as ex:
        assert(isinstance(ex, DataCaptureException))


def test_statistics_app_between():
    instance = Helper.load_data()
    stats = instance.build_stats()
    output = stats.between(1, 6)
    assert(output > 0)
    assert(isinstance(output, int) is True)


def test_statistics_app_errors():
    instance = Helper.load_data()
    stats = instance.build_stats()
    incorrect_values = ['a', -10, False, 9999]
    errors = []
    for value in incorrect_values:
        for method in [stats.less, stats.greater, instance.add, ]:
            try:
                _ = method(value)
            except DataCaptureException as ex:
                errors.append(ex)
    assert(all([isinstance(err, DataCaptureException) for err in errors]))
