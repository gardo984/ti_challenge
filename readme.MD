# TI Code Challenge

The repository contains code related to a `Code Challenge` for the company `Team International`.

Details about the code challenge are in the file **`TECH_CHALLENGE_PYTHON_SENIOR (1).pdf`**.

## Tests

- Create a virtualenv:
```sh
mkvirtualenv ti-challenge -p $(which python3)
```
- Install required packages for unit tests:
```sh
pip3 install -r requirements.txt
```
- Run unit tests:
```sh
pytest -v statistics_tests.py
```
- Get out of the virtualenv:
```sh
deactivate
```
- Remove Virtualenv:
```sh
rmvirtualenv ti-challenge
```

## Run application

- To run app:
```sh
python3 statistics_tests.py
```