
from typing import List, Sequence

DATA_CAPTURE_FROM: int = 0
DATA_CAPTURE_TO: int = 1000


class DataCaptureException(Exception):
    """ Customized Exception """
    pass


class DataCaptureStats(object):
    """
    Class in charge to perform the statistic operations.
    """

    def __init__(self, data: Sequence[int]):
        self.data = self._sort_data(data)

    def _sort_data(self, data: Sequence[int]) -> List[int]:
        """ Sort data """
        return sorted(data, reverse=False)

    def _validate_input(self, value: int) -> None:
        if not isinstance(value, int):
            raise DataCaptureException(
                'Invalid value, value must be a positive integer.'
            )

    def less(self, value: int) -> int:
        """
        Get the quantity of values less than the specified parameter.
        """
        self._validate_input(value)
        return len(
            list(filter(
                lambda x: x < value, self.data
            ))
        )

    def greater(self, value: int) -> int:
        """
        Get the quantity of values greater than the specified parameter.
        """
        self._validate_input(value)
        return len(
            list(filter(
                lambda x: x > value, self.data
            ))
        )

    def between(self, from_value: int, to_value: int) -> int:
        """
        Get the quantity of values between the specified parameters.
        """
        for value in [from_value, to_value]:
            self._validate_input(value)

        if from_value > to_value:
            raise DataCaptureException(
                'first argument must be lower than second one.'
            )

        return len(
            [x for x in self.data if x >= from_value and x <= to_value]
        )


class DataCapture(object):
    """
    Class in charge to collect int values
    and perform statistics operations.
    """

    data: List = []

    def __init__(self):
        pass

    def add(self, value: int) -> None:
        if not isinstance(value, int):
            raise DataCaptureException(
                'Invalid value, argument must an integer.'
            )

        if value < 0:
            raise DataCaptureException(
                'Invalid value, value must be on range from {} to {}.'.format(
                    DATA_CAPTURE_FROM,
                    DATA_CAPTURE_TO
                )
            )

        self.data.append(value)

    def build_stats(self) -> DataCaptureStats:
        item = DataCaptureStats(self.data)
        return item


if __name__ == '__main__':

    options = [3, 9, 3, 4, 6]
    instance = DataCapture()
    for item in options:
        instance.add(item)
    stats = instance.build_stats()

    print(stats.less(4))
    print(stats.between(3, 6))
    print(stats.greater(4))
